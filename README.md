# Work in progress

I only uploaded this because I had cleaned up all my personal info. But it's probably not ready for use yet.

# Roles

Relies on some custom made roles found in my gitlab account.

  - [prometheus](https://gitlab.com/stemid-ansible/prometheus.git)
  - [collectd-exporter](https://gitlab.com/stemid-ansible/collectd-exporter.git)
  - [blackbox-exporter](https://gitlab.com/stemid-ansible/blackbox-exporter.git)
  - [snmp-exporter](https://gitlab.com/stemid-ansible/snmp-exporter.git)
  - [pushgateway](https://gitlab.com/stemid-ansible/pushgateway.git)
  - [alertmanager](https://gitlab.com/stemid-ansible/alertmanager.git)

# Configure

## 1. Create a hosts inventory

Its name should match your environment, start by copying the default inventory and edit it.

    $ cp inventory/hosts.default inventory/hosts.myenv

And edit it to match your environment. The first variable called ``prometheus_environment`` must match the name of your vars file.

## 2. Create a vars file for your environment

Same here, start by copying the defaults.

    $ cp vars/default.yml vars/myenv.yml

Now edit all the variables inside to match your environment.

# Run

    ansible-playbook -i inventory/hosts.myenv site.yml

# Customize

## 1. Add host vars

See ``inventory/host_vars/example.localhost.yml`` for an example of defining collectd plugins on agents.
